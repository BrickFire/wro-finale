package de.smarthive.controller;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.robotics.Color;

public class Arm extends EV3LargeRegulatedMotor {

    private final int lowered = -140;
    private final int elevated = 0;
    private final int placed = 0;

    private final int[] depot = new int[3];
    private int currentIndexOfDepot = 0;

    public final int speed = 200;
    private final double distance = 6;

    public Arm(Port port) {
        super(port);
        for (int i = 0; i < depot.length; i++) {
            depot[i] = Color.NONE;
        }
    }

    public int getCurrentIndexOfDepot(){ return currentIndexOfDepot;}

    public void lower() {
        lower(false);
    }

    public void lower(boolean immediateReturn) {
        setSpeed(speed);
        rotateTo(lowered, immediateReturn);
    }

    public void elevate() {
        elevate(false);
    }

    public void elevate(boolean immediateReturn) {
        setSpeed(speed);
        rotateTo(elevated, immediateReturn);
    }

    public void place() {
        place(false);
    }

    public void place(boolean immediateReturn) {
        rotateTo(placed, immediateReturn);
    }

    public void add(int color) {
        if (currentIndexOfDepot <= depot.length - 1)
            depot[currentIndexOfDepot++] = color;
    }

    public void release(double distance) {
        release(distance, false);
    }

    public void release(double distance, boolean immediateReturn) {
        //todo: Altlast von Adrian zu Robot verschieben
        if (currentIndexOfDepot >= 0) {
            Robot.setDefaultSpeedAndAcceleration();
            double calcDistance = distance - this.distance * (2 - currentIndexOfDepot);
            Robot.pilot.travel(calcDistance);
            place(immediateReturn);
            Robot.pilot.travel(calcDistance * -1);
            elevate(immediateReturn);
            depot[currentIndexOfDepot--] = Color.NONE;
        }
    }

    public int[] getDepot() {
        return depot;
    }
}

package de.smarthive.controller;

import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.Port;

public class Kicker extends EV3MediumRegulatedMotor {

    private final int opened = 200;
    private final int closed = 0;
    private final int holded = 30;

    private final int speed = 200;

    public Kicker(Port port) {
        super(port);
    }

    public void open() {
        open(false);
    }

    public void open(boolean immediateReturn) {
        setSpeed(speed);
        rotateTo(opened, immediateReturn);
    }

    public void close() {
        close(false);
    }

    public void close(boolean immediateReturn) {
        setSpeed(speed);
        rotateTo(closed, immediateReturn);
    }
}

package de.smarthive.controller;

import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.Color;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;

public class Robot {

    public final static Wheel wheelLeft = WheeledChassis.modelWheel(new EV3LargeRegulatedMotor(MotorPort.B), 8.2).offset(-7.6252);
    public final static Wheel wheelRight = WheeledChassis.modelWheel(new EV3LargeRegulatedMotor(MotorPort.C), 8.2).offset(7.6252);
    public final static Chassis chassis = new WheeledChassis(new Wheel[] { wheelRight, wheelLeft }, WheeledChassis.TYPE_DIFFERENTIAL);
    public final static MovePilot pilot = new MovePilot(chassis);

    public final static EV3ColorSensor colorSLeft = new EV3ColorSensor(SensorPort.S2);
    public final static EV3ColorSensor colorSRight = new EV3ColorSensor(SensorPort.S3);

    public final static EV3ColorSensor colorS = new EV3ColorSensor(SensorPort.S1);

    public final static EV3UltrasonicSensor us = new EV3UltrasonicSensor(SensorPort.S4);

    public final static Arm arm = new Arm(MotorPort.D);
    public final static Kicker kicker = new Kicker(MotorPort.A);


    public final static double defaultLinearSpeed = pilot.getMaxLinearSpeed() - 3;
    public final static double defaultLinearAcceleration = 20;
    public final static double defaultAngularSpeed = pilot.getMaxAngularSpeed() - 150;
    public final static double defaultAngularAcceleration = pilot.getMaxAngularSpeed()/2;

    public final static float coolingUnitDistance = 0.26f;

    public static void setDefaultSpeedAndAcceleration() {
        pilot.setLinearSpeed(defaultLinearSpeed);
        pilot.setLinearAcceleration(defaultLinearAcceleration);
        pilot.setAngularSpeed(defaultAngularSpeed);
        pilot.setAngularAcceleration(defaultAngularAcceleration);
    }

    public static boolean seeCoolingUnit() {
        float distance = getUltrasonicDistance();
        System.out.println("Distance to Cooling unit: " + distance);

        return distance <= coolingUnitDistance;
    }

    public static float getUltrasonicDistance(){
        float[] max = new float[1];
        us.getDistanceMode().fetchSample(max,0);

        return max[0];
    }

    public static void align(boolean forward) {
        Sound.beep();
        LCD.drawInt(colorSLeft.getColorID(), 0, 0);
        findLine(forward, Color.BLACK);
        findLine(!forward,Color.WHITE);
        findLine(forward, Color.BLACK);
    }

    private static void findLine(boolean forward, int color){
        System.out.println("Forward?: " + forward + " Color: "+ color);
        wheelLeft.getMotor().setSpeed(50);
        wheelRight.getMotor().setSpeed(50);
        if(forward) {
            wheelLeft.getMotor().forward();
            wheelRight.getMotor().forward();
        }else {
            wheelLeft.getMotor().backward();
            wheelRight.getMotor().backward();
        }

        boolean alignedLeft = false;
        boolean alignedRight = false;

        while (!(alignedLeft && alignedRight)) {
            if (colorSLeft.getColorID() == color) {
                wheelLeft.getMotor().stop(true);
                alignedLeft = true;
            }
            if (colorSRight.getColorID() == color) {
                wheelRight.getMotor().stop(true);
                alignedRight = true;
            }
        }
    }

    public static double addCoolingUnit(double distanceToUnit, int color, boolean immediateReturn){
        double driveToZero = -17 + distanceToUnit;
        double driveToUnit = 6 * (4 - arm.getCurrentIndexOfDepot());

        Robot.pilot.travel(driveToZero, immediateReturn);
        arm.lower(immediateReturn);

        Robot.pilot.travel(driveToUnit, immediateReturn);
        arm.elevate(immediateReturn);

        arm.add(color);
        return driveToZero + driveToUnit;
    }

    public static void travel(double distance, boolean stop){
        pilot.forward();
		while (pilot.getMovement().getDistanceTraveled() < distance);
		if (stop)
		    pilot.stop();
    }

    public static double getDistanceTraveled(){
        return pilot.getMovement().getDistanceTraveled();
    }

    public static double driveToCoolingUnit(double toTravel, String outMessage){
        while (!seeCoolingUnit() && getDistanceTraveled() < toTravel); //Warte bis CoolingUnit gesehen wird
        System.out.println(outMessage + getDistanceTraveled()); //Gibt die gefahrene Strecke aus

        return getDistanceTraveled(); //Gibt die gefahrene Strecke zurück
    }

    public static double pickupCoolingUnit(double seeDistance, double toTravel, String outMessage){
        if (seeDistance < toTravel) {
            double distance = addCoolingUnit(14 - seeDistance, 0, false); //Hebt die CoolingUnit auf
            System.out.println(outMessage + distance); //Gibt die gefahrene Strecke aus

            return distance; //Gibt die gefahrene Strecke zurück
        }
        else
            pilot.stop();

        System.out.println(outMessage + 0); //Gibt die gefahrene Strecke aus
        return 0;
    }
}

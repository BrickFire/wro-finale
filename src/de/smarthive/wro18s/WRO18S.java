package de.smarthive.wro18s;

import de.smarthive.controller.Robot;
import lejos.hardware.Sound;
import lejos.utility.Delay;
//import lejos.hardware.Button;

public class WRO18S extends Robot {
	public static void main(String[] args) {
		Sound.twoBeeps();
		//Button.waitForAnyPress();
		setDefaultSpeedAndAcceleration();

		//todo: Arm komplett hochfahren

		kicker.open(true); //Kicker öffnen
		seeCoolingUnit(); //Start des Sensors

		double move; //Alter Wert

		double toTravel;
		double pickupDistance;
		double seeDistance;

		pilot.travel(37); //Aus der Base fahren

		/*--- Grüne CoolingUnit: ---*/

		pilot.rotate(-90); //Zu grüner Fabrik drehen
		pilot.setLinearSpeed(15); //Fährt langsamer, um besser sehen zu können
		pilot.forward(); //Auf grüne Fabrik zufahren

		toTravel = 10; //Maximal 10cm nach vorne fahren
		seeDistance = driveToCoolingUnit(toTravel, "Green: Move to see: "); //Fährt zur grünen CoolingUnit
		setDefaultSpeedAndAcceleration(); //Setzt den Speed zurück
		pickupDistance = pickupCoolingUnit(seeDistance, toTravel, "Green: Move to Unit : "); //Hebt die grüne CoolingUnit auf

		/*--- Blaue CoolingUnit: ---*/

		pilot.rotate(180); //Zu blauer Fabrik drehen
		pilot.setLinearSpeed(15); //Fährt langsamer, um besser sehen zu können
		pilot.forward(); //Auf blaue Fabrik zufahren

		toTravel = seeDistance + pickupDistance + 10; //Maximal zurück zur (Mitte + 10cm) fahren
		seeDistance = driveToCoolingUnit(toTravel, "Green: Move to see: "); //Fährt zur blauen CoolingUnit
		setDefaultSpeedAndAcceleration(); //Setzt den Speed zurück
		pickupDistance = pickupCoolingUnit(seeDistance, toTravel, "Blue: Move to Unit : "); //Hebt die blauen CoolingUnit auf

		/*--- Neue CoolingUnit suchen: ---*/

		pilot.rotate(-90);
		if (seeCoolingUnit()) {

			/*--- Gelbe CoolingUnit: ---*/

			pilot.rotate(-90);
			//travel((move2 - move), false);
			pilot.arc(15, 180);
			//pilot.travel((move2 - move));

			pilot.setLinearSpeed(15);
			pilot.forward();
			while (!seeCoolingUnit() && pilot.getMovement().getDistanceTraveled() < 10) ;
			move = pilot.getMovement().getDistanceTraveled();
			System.out.println("1: Move to see: " + move);
			setDefaultSpeedAndAcceleration();
			if (move < 10)
				move += addCoolingUnit(14 - move, 0, false);
			else
				pilot.stop();
			System.out.println("1: Move to Unit : " + move);
			pilot.rotate(180);
		}

//		pilot.rotate(-90);
//
//		pilot.travel(-10 - move);
//
//
//		pilot.travel(34);
//
//		pilot.rotate(-90);
//		pilot.travel(10);
//
//		//Vor Roter CoolingUnit
//		if (seeCoolingUnit())
//			move = addCoolingUnit(6.5, 0, false);
//
//		pilot.rotate(180);
//
//		if(arm.getCurrentIndexOfDepot() != 3) {
//			pilot.travel(20 + move);
//
//			//Vor Gelber CoolingUnit
//			if (seeCoolingUnit())
//				move = addCoolingUnit(6.5, 0, false);
//
//			pilot.travel(-10 - move);
//		} else {
//			pilot.travel(10 + move);
//		}
//
//		pilot.rotate(-90);

		kicker.close();

		Delay.msDelay(4000);
	}
}

